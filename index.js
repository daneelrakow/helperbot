const Slackbot = require('slackbots')
const axios = require('axios')
const fs = require('fs');

let your_path_to_file = '';

const OAUTH_TOKEN = '';

var timeTabe = {'users': []}
var content;

function getKey() {
  
  fs.readFile('./keys.json', function(err,data){
    console.log("Reading OAUTH token now.")
    content = JSON.parse(data);
    content = content.keys.slack;
    console.log(content);
  })
}

const bot = new Slackbot({
  token: OAUTH_TOKEN,
  name: 'helperbot'
})

// Start Handler
bot.on('start', () => {
  getKey();

  const params = {
    icon_emoji: ':smiley:'
  };

  bot.postMessageToChannel(
    'helper',
    'Fear not! Helperbot is here!',
    params
  );
});

// Error Handler
bot.on('error', err => console.log(err));

// Message Handler
bot.on('message', data => {
  if (data.type !== 'message') {
    return;
  }

  handleMessage(data.text);
});

// Responds to Data
function handleMessage(message) {
  if (message == 'in') {
    bot.postMessageToChannel('helper', 'clocked in at ' + returnTime());
  }

  if (message == 'out') {
    bot.postMessageToChannel('helper', 'clocked out at ' + returnTime());
  }
}

//returns time
function returnTime() {
  var d = new Date();

  var time = d.getHours() + ":" + d.getMinutes() + " on " + (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
  return time;
}
